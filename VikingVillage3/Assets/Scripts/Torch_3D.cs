﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch_3D : MonoBehaviour
{
    [FMODUnity.EventRef] 
    public string FmodEvent_torch;


    // Start is called before the first frame update
    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShot(FmodEvent_torch, gameObject.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
