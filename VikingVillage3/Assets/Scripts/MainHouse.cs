﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainHouse : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string FmodEvent_music;


    // Start is called before the first frame update
    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShot(FmodEvent_music, gameObject.transform.position);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

