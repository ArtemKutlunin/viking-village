﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag_3D : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string FmodEvent_flag;


    // Start is called before the first frame update
    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShot(FmodEvent_flag, gameObject.transform.position);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
