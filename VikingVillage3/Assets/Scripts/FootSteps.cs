﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using Invector.vCharacterController; 

public class FootSteps : MonoBehaviour 
{
    [FMODUnity.EventRef] public string FootEvent; //переменная для шагов 
    [FMODUnity.EventRef] public string runEvent; // переменная для бега 
    FMOD.Studio.EventInstance eventInstance;
    vThirdPersonInput tpInput; //переменная, которая фиксит баг шагов, проверяет магнитуду 
    vThirdPersonController cheking; // переменная, на проверку бега или шага 

    // Start is called before the first frame update
    void Start()
    {
       tpInput = GetComponent<vThirdPersonInput>();
        cheking = GetComponent<vThirdPersonController>();
    }

    // Update is called once per frame
    
    void FootStep()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            if (cheking.isSprinting)
            {

                eventInstance = RuntimeManager.CreateInstance(runEvent);
                RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
                eventInstance.start();
                eventInstance.release();
            }
            
            else
            {
                eventInstance = RuntimeManager.CreateInstance(FootEvent);
                RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
                eventInstance.start();
                eventInstance.release();
            }
        }
   
        
    }
   
}