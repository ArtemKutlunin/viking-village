﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship_3D: MonoBehaviour
{
    [FMODUnity.EventRef]
    public string FmodEvent_ship;


    // Start is called before the first frame update
    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShot(FmodEvent_ship, gameObject.transform.position);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
